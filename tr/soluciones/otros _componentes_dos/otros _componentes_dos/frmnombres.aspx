﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmnombres.aspx.cs" Inherits="otros__componentes_dos.frmnombres" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 205px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>Nombres</td>
                <td>
                    <asp:DropDownList ID="Ddl_nombres" runat="server" OnSelectedIndexChanged="Ddl_nombres_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Btn_adicionar" runat="server" OnClick="Btn_adicionar_Click" Text="Adicionar" />
                </td>
                <td>
                    <asp:TextBox ID="Txt_nombre" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Btn_eliminar" runat="server" Text="Eliminar" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Lbl_mensaje" runat="server"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
