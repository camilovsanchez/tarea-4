﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmencuesta.aspx.cs" Inherits="otros_componentes.frmencuesta" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 456px;
        }
        .auto-style3 {
            width: 456px;
            height: 23px;
        }
        .auto-style4 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Bebida</td>
                <td>
                    <asp:DropDownList ID="ddlbebida" runat="server">
                        <asp:ListItem>Gaseosa</asp:ListItem>
                        <asp:ListItem>Jugo</asp:ListItem>
                        <asp:ListItem>Cerveza</asp:ListItem>
                        <asp:ListItem>Whiskey</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Estado civil</td>
                <td class="auto-style4">
                    <asp:RadioButtonList ID="rblestado" runat="server" Height="24px">
                        <asp:ListItem>Soltero</asp:ListItem>
                        <asp:ListItem>Casado</asp:ListItem>
                        <asp:ListItem>Viudo</asp:ListItem>
                        <asp:ListItem>Laico</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Edad</td>
                <td>
                    <asp:CheckBox ID="ckbedad" runat="server" Text="Mayor de edad" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Género</td>
                <td>
                    <asp:CheckBox ID="ckbgenero" runat="server" Text="Hombre" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnpre1" runat="server" OnClick="btnpre1_Click" Text="Pregunta 1" />
                </td>
                <td>
                    <asp:Label ID="lblpreg1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnpre2" runat="server" OnClick="btnpre2_Click" Text="Pregunta 2" />
                </td>
                <td>
                    <asp:Label ID="lblpreg2" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
