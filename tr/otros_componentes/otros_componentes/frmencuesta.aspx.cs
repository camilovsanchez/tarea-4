﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace otros_componentes
{
    public partial class frmencuesta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnpre1_Click(object sender, EventArgs e)
        {
            if (ckbgenero.Checked && ckbedad.Checked && rblestado.SelectedIndex == 0 && ddlbebida.SelectedIndex == 2)
                lblpreg1.Text = "Cumple condiciones";
            else
            
                lblpreg1.Text = "No cumple condiciones"; 
        }

        protected void btnpre2_Click(object sender, EventArgs e)
        {
            if (ckbgenero.Checked == false && ckbedad.Checked == false && rblestado.SelectedIndex == 2 && ddlbebida.SelectedIndex == 0)
                lblpreg2.Text = "Cumple condiciones";
            else
                lblpreg2.Text = "No cumple condiciones";
        }
    }
}