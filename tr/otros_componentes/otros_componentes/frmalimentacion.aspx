﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmalimentacion.aspx.cs" Inherits="otros_componentes.frmalimentacion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 459px;
        }
        .auto-style3 {
            width: 459px;
            height: 120px;
        }
        .auto-style4 {
            height: 120px;
        }
        .auto-style5 {
            width: 459px;
            height: 64px;
        }
        .auto-style6 {
            height: 64px;
        }
        .auto-style7 {
            width: 459px;
            height: 23px;
        }
        .auto-style8 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style5">
                    <asp:DropDownList ID="ddlplatofuerte" runat="server" OnSelectedIndexChanged="ddlplatofuerte_SelectedIndexChanged">
                        <asp:ListItem>Plato Fuerte</asp:ListItem>
                        <asp:ListItem>Desayuno</asp:ListItem>
                        <asp:ListItem>Almuerzo</asp:ListItem>
                        <asp:ListItem>Cena</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="lblplatofuerte" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Bebida<asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                    <asp:ListItem>Cerveza</asp:ListItem>
                    <asp:ListItem>Jugo</asp:ListItem>
                    <asp:ListItem>Gaseosa</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="auto-style4">
                    <asp:Label ID="lblbebida" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:CheckBox ID="cbiva" runat="server" Text="Iva" />
                </td>
                <td>
                    <asp:Label ID="lbliva" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:CheckBox ID="cbpropina" runat="server" Text="Propina" />
                </td>
                <td>
                    <asp:Label ID="lblpropina" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">Total</td>
                <td class="auto-style8">
                    <asp:Label ID="lbltotal" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">
                    <asp:Button ID="btnlimpiar" runat="server" Text="Limpiar" />
                    <asp:Button ID="btntotal" runat="server" Text="Calcular total" />
                </td>
                <td class="auto-style8">&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
