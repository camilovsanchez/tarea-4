﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace otros_componentes
{
    public partial class otros_componentes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtnombre.Focus();
            }

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Btnadicionar_Click(object sender, EventArgs e)
        {

            if (txtnombre.Text.Trim() == "")
            {
                Lblmensaje.Text = "Digite Un Nombre";
                txtnombre.Focus();
            }
            else
            {
                Lblmensaje.Text = "";
                Ddlnombres.Items.Add(txtnombre.Text);
                txtnombre.Text = "";
                txtnombre.Focus();
            }
        }

        protected void Btneliminar_Click(object sender, EventArgs e)
        {
            if (Ddlnombres.Items.Count == 0)
            {
                Lblmensaje.Text = "No hay nombres a eliminar";
                txtnombre.Focus();
            }
            else
            {
                Lblmensaje.Text = "";
                Ddlnombres.Items.Remove(Ddlnombres.SelectedItem.Value);
                txtnombre.Focus();
            }
        }
    }
}


