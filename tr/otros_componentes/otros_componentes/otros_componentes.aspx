﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="otros_componentes.aspx.cs" Inherits="otros_componentes.otros_componentes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 361px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Nombres</td>
                <td>
                    <asp:DropDownList ID="Ddlnombres" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="Btnadicionar" runat="server" OnClick="Btnadicionar_Click" Text="Adicionar" Width="112px" />
                </td>
                <td>
                    <asp:TextBox ID="txtnombre" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="Btneliminar" runat="server" Text="Eliminar" Width="110px" OnClick="Btneliminar_Click" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Lblmensaje" runat="server" Text="Label"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
