﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace vector_enteros
{
    public partial class FrmPositivos : System.Web.UI.Page
    {
        ClsEnteros objnumeros = new ClsEnteros();
        int cant, canpos, acum_pospar;

        protected void BtnPares_Click(object sender, EventArgs e)
        {
           
            cant = Convert.ToInt32(TxtCantidad.Text);
            int[] numeros = new int[cant];
            numeros = objnumeros.llenar_vector(cant);
            canpos = objnumeros.cantidad_positivos(cant, numeros);
            LblPositivos.Text = "" + canpos;
            acum_pospar = objnumeros.posiciones_pares(cant, numeros);
            LblPares.Text = "" + acum_pospar;


            
        }

       

        protected void Page_Load(object sender, EventArgs e)
        {

        }

    }
}