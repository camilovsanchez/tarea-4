﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmPositivos.aspx.cs" Inherits="vector_enteros.FrmPositivos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 649px;
        }
        .auto-style3 {
            width: 649px;
            height: 23px;
        }
        .auto-style4 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">Inserte el tamaño del vector</td>
                <td class="auto-style4">
                    <asp:TextBox ID="TxtCantidad" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Cantidad de positivos</td>
                <td class="auto-style4">
                    <asp:Label ID="LblPositivos" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Suma de elementos de posiciones pares</td>
                <td>
                    <asp:Label ID="LblPares" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="BtnPares" runat="server" OnClick="BtnPares_Click" Text="Calcular Pares" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
