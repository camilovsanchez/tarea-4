﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vector_enteros
{
    //Hacer un programa que lea un vector de numeros enteros de tamaño n y luego imprima: cantidad de positivos y suma de elementos posiciones pares
    public class ClsEnteros
    {
        int k;
        public int [] llenar_vector (int cant)
        {
            {
                int[] num = new int[cant];
                Random rnd = new Random();
                for (k = 0; k < cant; k++)
                {
                    num[k] = rnd.Next(-1000, 1000);
                }
            }
        }
         public int cantidad_positivos (int cant, int [] num1)
        {
            int canpos = 0;
            for (k = 0; k < cant; cant++)
            {
                if (num1[k] > 0)
                    canpos++;
            }
            return canpos;
        }

        public int posiciones_pares (int cant, int[] num2)
        {
            int acum_pospar = 0;
            for (k = 0; k < cant; k ++)
            {
                if (k % 2 == 0)
                    acum_pospar = acum_pospar + num2[k];

            }
            return acum_pospar;
        }

    
}