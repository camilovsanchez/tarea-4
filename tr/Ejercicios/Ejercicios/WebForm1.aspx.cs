﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejercicios
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Resultado_Click(object sender, EventArgs e)
        {
            //Variables
            double area, altura, base1;

            //Lectura de datos

            area = Convert.ToDouble(txtarea.Text);
            altura = Convert.ToDouble(txtaltura.Text);

            //Operación

            base1 = area * 2 / altura;

            //Imprimir
            lblbase.Text = Convert.ToString(base1);


        }
    }
}