﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Ejercicios.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>Area</td>
                <td>
                    <asp:TextBox ID="txtarea" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Altura</td>
                <td>
                    <asp:TextBox ID="txtaltura" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Base</td>
                <td class="auto-style2">
                    <asp:Label ID="lblbase" runat="server" Text="Resultado"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnresultado" runat="server" OnClick="Resultado_Click" Text="Resultado" />
                </td>
                <td>
                   
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
