﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Frm_ejercicio.aspx.cs" Inherits="WebApplication1.Frm_ejercicio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 240px;
            margin-top: 0px;
            margin-bottom: 94px;
        }
        .auto-style2 {
            width: 100%;
            height: 206px;
        }
        .auto-style3 {
            width: 100%;
            height: 243px;
        }
        .auto-style4 {
            width: 100%;
        }
    </style>
</head>
<body style="width: 819px; height: 286px;">
   
    <h2>Ejercicio 1</h2>
    
     <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>Área </td>
                <td>
                    <asp:TextBox ID="Txt_area" runat="server" Height="16px" ToolTip="Ingrese Número"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Altura</td>
                <td>
                    <asp:TextBox ID="Txt_altura" runat="server" Height="16px" ToolTip="Ingrese Número"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Base</td>
                <td>
                    <asp:Label ID="Lblbase" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_calculo" runat="server" OnClick="Btn_calculo_Click" Text="CALCULO" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>

    <h2>Ejercicio 2</h2>
    

    <table class="auto-style2">
        <tr>
            <td>Salario Mensual</td>
            <td>
                <asp:TextBox ID="Txtsal_m" runat="server" ToolTip="Ingrese valores"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Ingresos Extra Mensuales</td>
            <td>
                <asp:TextBox ID="Txting_m" runat="server" ToolTip="Ingrese valores"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Gastos Mensuales</td>
            <td>
                <asp:TextBox ID="Txtgast_m" runat="server" ToolTip="Ingrese valores"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Dinero que usted puede ahorrar en un año</td>
            <td>
                <asp:Label ID="Lblahoro" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_calculo2" runat="server" OnClick="btn_calculo2_Click" Text="HACER CALCULO" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <h2>Ejercicio 3</h2>
       
        <table class="auto-style3">
            <tr>
                <td>Cantidad de taxis</td>
                <td>
                    <asp:TextBox ID="Txt_taxis" runat="server" ToolTip="Ingrese valores"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Cantidad de buses </td>
                <td>
                    <asp:TextBox ID="Txt_buses" runat="server" ToolTip="Ingrese valores"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Cantidad de particulares</td>
                <td>
                    <asp:TextBox ID="Txt_part" runat="server"  ToolTip="Ingrese valores"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Cantidad de motos</td>
                <td>
                    <asp:TextBox ID="Txt_motos" runat="server"  ToolTip="Ingrese valores"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Ganancias por taxis</td>
                <td>
                    <asp:Label ID="Lbltaxis" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Ganancias por&nbsp; buses </td>
                <td>
                    <asp:Label ID="Lblbuses" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Ganancias por&nbsp; particulares</td>
                <td>
                    <asp:Label ID="Lblpart" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Ganancias por&nbsp; motos</td>
                <td>
                    <asp:Label ID="Lblmotos" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Ganancias brutas</td>
                <td>
                    <asp:Label ID="Lblbrutas" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_calculo3" runat="server" OnClick="btn_calculo3_Click" Text="HACER CALCULO" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>

          <h2>Ejercicio 4</h2>

         <table class="auto-style4">
             <tr>
                 <td>Ingrese Nombre 1</td>
                 <td>
                     <asp:TextBox ID="Txt_nm1" runat="server"></asp:TextBox>
                 </td>
                 <td>Ingese Edad 1</td>
                 <td>
                     <asp:TextBox ID="Txt_ed1" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td>Ingrese Nombre 2</td>
                 <td>
                     <asp:TextBox ID="Txt_nm2" runat="server"></asp:TextBox>
                 </td>
                 <td>Ingese Edad 2</td>
                 <td>
                     <asp:TextBox ID="Txt_ed2" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td>Ingrese Nombre 3</td>
                 <td>
                     <asp:TextBox ID="Txt_nm3" runat="server"></asp:TextBox>
                 </td>
                 <td>Ingese Edad 3</td>
                 <td>
                     <asp:TextBox ID="Txt_ed3" runat="server"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td>Promedio de edades </td>
                 <td>
                     <asp:Label ID="Lbledad" runat="server"></asp:Label>
                 </td>
                 <td>Nombres registrados</td>
                 <td>
                     <asp:Label ID="Lblnombres" runat="server"></asp:Label>
                 </td>
             </tr>
             <tr>
                 <td>
                     <asp:Button ID="Btn_calculo4" runat="server" Text="CALCULO" OnClick="Btn_calculo4_Click" />
                 </td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
                 <td>&nbsp;</td>
             </tr>
         </table>
    </form>

   
</body>
</html>
