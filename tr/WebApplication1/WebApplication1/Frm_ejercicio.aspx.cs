﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Frm_ejercicio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Btn_calculo_Click(object sender, EventArgs e)
        {
            //Definicion de variables locales
            double area, altura, base1;
           
            //lectura de los datos
            area = Convert.ToDouble(Txt_area.Text);
            altura = Convert.ToDouble(Txt_altura.Text);

            // operacion
            base1 = area * 2 / altura;

            //imprimir
            Lblbase.Text = Convert.ToString(base1);

        }

        protected void btn_calculo2_Click(object sender, EventArgs e)
        {

            //Definicion de variables locales
            double salario, extras, gastos, ahorro;

            //lectura de los datos
            salario = Convert.ToDouble(Txtsal_m.Text);
            extras = Convert.ToDouble(Txting_m.Text);
            gastos = Convert.ToDouble(Txtgast_m.Text);

            // operacion
            ahorro = salario + extras - gastos;

            //imprimir
            Lblahoro.Text = Convert.ToString(ahorro * 12);






        }

        protected void btn_calculo3_Click(object sender, EventArgs e)
        {
            //Definicion de variables locales
            int taxis, buses, particulares, motos, ganancias_b2, ganancias_t, ganancias_b, ganancias_p, ganancias_m;

            //lectura de los datos
            taxis = Convert.ToInt16(Txt_taxis.Text);
            buses = Convert.ToInt16(Txt_buses.Text);
            particulares = Convert.ToInt16(Txt_part.Text);
            motos = Convert.ToInt16(Txt_motos.Text);

            // operacion
            ganancias_t = taxis * 3000;
            ganancias_b = buses * 10000;
            ganancias_p = particulares * 5000;
            ganancias_m = motos *2000;
            ganancias_b2 = ganancias_t + ganancias_b + ganancias_p + ganancias_m;

            //imprimir
            Lbltaxis.Text = Convert.ToString(ganancias_t);
            Lblbuses.Text = Convert.ToString(ganancias_b);
            Lblpart.Text = Convert.ToString(ganancias_p);
            Lblmotos.Text = Convert.ToString(ganancias_m);
            Lblbrutas.Text = Convert.ToString(ganancias_b2);
        




        }

        protected void Btn_calculo4_Click(object sender, EventArgs e)
        {
            //Definicion de variables locales
            double ed1, ed2, ed3, promedio;
            string nom1, nom2, nom3;

            //lectura de los datos
            ed1 = Convert.ToDouble(Txt_ed1.Text);
            ed2 = Convert.ToDouble(Txt_ed2.Text);
            ed3 = Convert.ToDouble(Txt_ed3.Text);
            nom1 = Txt_nm1.Text;
            nom2 = Txt_nm2.Text;
            nom3 = Txt_nm3.Text;

            // operacion
            promedio = (ed1 + ed2 + ed3) / 3;

            //imprimir
            Lbledad.Text = Convert.ToString(promedio);
            Lblnombres.Text = "Hermano 1: " + nom1 + ", Hermano 2: " + nom2 + ", Hermano 3: " + nom3;




        }
    }
}