﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vector_numero_entero
{
    public class ClsEjercicioDos
    {
        public int[] llenar_vector (int n)
        {
            //Definicion de vectores

            Random rnd = new Random();
            int[] numero = new int[n];
            int j;
            for (j = 0; j < n; j++)
            {
                numero[j] = rnd.Next(100);
            }

            return numero;
        }

        public int hallar_mayor(int [] numero, int n)
        {
            //Definición de vectores
            int k, mayor;

            mayor = numero[0];

            for (k=0; k<n; k++)
            {
                if (numero[k] > mayor)
                {
                    mayor = numero[k];
                }
            }
            return mayor;
        }

        public int primer_par (int [] numero, int n)
        {
            //definición de vectores
            int k =0, pos, sw = 0;

            while (k < n && sw ==0)
            {
                if (numero[k] % 2 == 0)
                    sw = 1;
                else
                    k++;
            }
            if (sw == 0)
            {
                pos = -1;
            }
            else
            {
                pos = k;
            }
            return pos;

        }

    }
}