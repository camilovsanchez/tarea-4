﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace punto_cuatro_taller
{
    public partial class FrmNomina : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btncalcular_Click(object sender, EventArgs e)
        {
            //Definición de variables

            ClsPuntoCuatro objPuntoCuatro = new ClsPuntoCuatro();
            double salbasico, ded, aux, salneto;

            //Lectura de datos

            salbasico = Convert.ToDouble(txtbasico.Text);

            //Operación
            ded = objPuntoCuatro.hallar_deducciones(salbasico);
            aux = objPuntoCuatro.hallar_auxilio_transporte(salbasico);
            salneto = objPuntoCuatro.hallar_salario_neto(salbasico, ded, aux);

            //Imprimir resultados
            lbldeducciones.Text = "" + ded;
            lblauxiliot.Text = "" + aux;
            lblneto.Text = "" + salneto;
        }
    }
}