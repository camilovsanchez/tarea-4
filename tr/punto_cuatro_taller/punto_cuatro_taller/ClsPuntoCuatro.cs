﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace punto_cuatro_taller
{
    public class ClsPuntoCuatro
    {
        public double hallar_deducciones (double salbasico)
        {
            double deducciones;
            if (salbasico <= 4 * 787000)
                deducciones = salbasico * 8 / 100;
            else
                deducciones = salbasico * 9 / 100;
            return deducciones;
        }

        public double hallar_auxilio_transporte (double salbas)
        {
            double auxt;
            if (salbas <= 2 * 787000)
                auxt = 82700;
            else
                auxt = 0;
            return auxt;
        }

        public double hallar_salario_neto(double sb, double d, double at)
        {
            double salneto;
            salneto = sb - d + at;
            return salneto;
        }
    }
}