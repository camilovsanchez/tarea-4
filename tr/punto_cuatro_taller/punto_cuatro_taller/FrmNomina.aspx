﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FrmNomina.aspx.cs" Inherits="punto_cuatro_taller.FrmNomina" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 693px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Salario básico</td>
                <td>
                    <asp:TextBox ID="txtbasico" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Deducciones</td>
                <td>
                    <asp:Label ID="lbldeducciones" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Auxilio de transporte</td>
                <td>
                    <asp:Label ID="lblauxiliot" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Salario neto</td>
                <td>
                    <asp:Label ID="lblneto" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btncalcular" runat="server" OnClick="btncalcular_Click" Text="Button" />
                </td>
                <td>
                    <asp:Label ID="lblmensaje" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
