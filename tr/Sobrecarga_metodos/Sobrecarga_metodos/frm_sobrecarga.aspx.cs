﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sobrecarga_metodos
{
    public partial class frm_sobrecarga : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TxtNumeroH.Text = "0";
                TxtValorH.Text = "0";
            }
        }

        protected void BtnCalcular_Click(object sender, EventArgs e)
        {
            //Declaración de variables
            double salneto, pord, porb;
            int numh, valh;
            ClsSalario objsalario = new ClsSalario();

            if(ddlTipo.SelectedIndex==1)
            {
                if (TxtNumeroH.Text.Trim() == "" || TxtValorH.Text.Trim() == "")
                {
                    LblMensaje.Text = "Digite el número de horas y el valor de la hora";
                    TxtNumeroH.Focus()
                      

                }
                else
                {
                    numh = Convert.ToInt32(TxtNumeroH.Text);
                    valh = Convert.ToInt32(TxtValorH.Text);
                    switch (RblDeducciones.SelectedIndex)
                    {
                        case 0: pord = 5;
                            break;
                        case 1: pord = 8;
                            break;
                        case 2: pord = 10;
                            break;
                    }

                    if(RblBonificaciones.SelectedIndex)
                }
            }
          
        }

        protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlTipo.SelectedIndex==1)
            {
                TxtNumeroH.Visible = true;
                TxtValorH.Visible = true;
                RblBonificaciones.Visible = true;
                RblDeducciones.Visible = true;
                TxtNumeroH.Focus();
            }
            else
            {
                if (ddlTipo.SelectedIndex==2)
                {
                    TxtNumeroH.Visible = true;
                    TxtValorH.Visible = false;
                    RblBonificaciones.Visible = false;
                    RblDeducciones.Visible = false;
                    TxtNumeroH.Focus();
                }
                else
                {
                    TxtNumeroH.Visible = false;
                    TxtValorH.Visible = false;
                    RblBonificaciones.Visible = false;
                    RblDeducciones.Visible = false;

                }
                
            }
            
        }

        protected void TxtNumeroH_TextChanged(object sender, EventArgs e)
        {
            BtnCalcular.Enabled = true;
        }
    }
}