﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sobrecarga_metodos
{
    public class ClsSalario
    {
        public double hallar_salario (int numh, int valh, double pocdes, double porbon)
        {
            double salbas, valded, valbon, salneto;
            salbas = numh * valh;
            valded = salbas * pocdes / 100;
            valbon = salbas * porbon / 100;
            salneto = salbas - valded + valbon;
            return salneto;
        }

        public double hallar_salario (int numh)
        {
            double salneto;
            salneto = numh * 6000;
            return salneto;

            
        }
    }
}