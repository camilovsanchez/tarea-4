﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frm_sobrecarga.aspx.cs" Inherits="Sobrecarga_metodos.frm_sobrecarga" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 660px;
        }
        .auto-style3 {
            width: 660px;
            height: 26px;
        }
        .auto-style4 {
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">Tipo de contrato</td>
                <td class="auto-style4">
                    <asp:DropDownList ID="ddlTipo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTipo_SelectedIndexChanged">
                        <asp:ListItem>Seleccione el tipo de contrato</asp:ListItem>
                        <asp:ListItem>Fijo</asp:ListItem>
                        <asp:ListItem>Temporal</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Número de horas</td>
                <td>
                    <asp:TextBox ID="TxtNumeroH" runat="server" MaxLength="3" OnTextChanged="TxtNumeroH_TextChanged" ToolTip="Número menor de 1000" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Valor hora</td>
                <td>
                    <asp:TextBox ID="TxtValorH" runat="server" MaxLength="8" ToolTip="Numérico" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Porcentaje deducciones</td>
                <td>
                    <asp:RadioButtonList ID="RblDeducciones" runat="server" OnSelectedIndexChanged="RblDeducciones_SelectedIndexChanged" Visible="False" Width="85px">
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Porcentaje bonificaciones</td>
                <td>
                    <asp:RadioButtonList ID="RblBonificaciones" runat="server" AutoPostBack="True" Visible="False">
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Salario neto</td>
                <td>
                    <asp:Label ID="LblNeto" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="BtnCalcular" runat="server" Enabled="False" OnClick="BtnCalcular_Click" Text="Calcular Neto" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="LblMensaje" runat="server" Font-Size="XX-Large"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
