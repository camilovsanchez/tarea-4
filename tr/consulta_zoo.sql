--Mostrar todos los zoologicos
SELECT * 
FROM zoologico

--Mostrar zoologicos por pais. LISTAR SOLO EL NOMBRE Y LA CIUDAD DE ESTOS
SELECT *
FROM zoologico
WHERE pais='Colombia'


-- LISTAR ESPECIES EN PELIGRO DE EXTINCION

SELECT *
FROM especies
WHERE peligro_extincion= 1


--Listar los zoo cuyo presupuesto no supera los 70 millones

SELECT *
FROM zoologico
WHERE presupuesto>100000000	


--  Listar los zoo que tengan un tama�o entre 4 y 6 km2
SELECT *
FROM zoologico
WHERE tamano >4 and tamano < 8

SELECT *
FROM zoologico
WHERE tamano BETWEEN 4 AND 8


-- LISTADO DE LOS ZOO POR CIUDAD EN ORDEN ALFABETICO
SELECT *
FROM zoologico
ORDER BY ciudad DESC


-- ORDENAR POR CIUDD Y NOMBRE
SELECT *
FROM zoologico
ORDER BY ciudad, nombre


-- LISTAR ZOOLOGICOS CUYO NOMBRE EMPIEZA POR "L"
SELECT *
FROM zoologico
WHERE nombre LIKE 'l%'

--COUNT cuenta los registros
-- SUM suma de valores de la columna
--AVG Promedia los valores de la columna
--MAX encuentra el valor m�ximo de la columna
--MIN encuentra el valor minimo de la columna

-- COUNT: cUENTE LOS REGISTROS DE LA TABLA ANIMALES

SELECT COUNT (*) AS [Total animales BD]
FROM animales

-- CUENTE LOS ZOO QUE TIENE COLOMBIA
SELECT COUNT (*) AS [Total zoo Colombia]
FROM zoologico
WHERE pais='Colombia'
-- TOTALIZAR EL PRESUPUESTO DE TODOS LOS ZOO
SELECT SUM (presupuesto) AS [Presupuesto Zool�gicos]
FROM zoologico

-- Promediar los presupuestos de los zoo
SELECT SUM (presupuesto)/ COUNT(id) AS [Promedio aritmetico], AVG(presupuesto) AS[promedio AVG]
FROM zoologico

-- Encontrar el Mayor presupuesto dado a un zoo
SELECT MAX(presupuesto) AS [MAYOR PRESUPUESTO]
FROM zoologico

-- mOSTRAR EL MENOR TAMA�O DE LOS ZOO
SELECT MIN (tamano) AS [MENOR TAMA�O]
FROM zoologico

-- MOSTRAR EL MAYOR Y MENOR A�O DE NACIMIENTO DE LOS ANIMALES
SELECT MAX(annio_nacimiento) AS [MAYOR A�O NACIMIENTO], MIN(annio_nacimiento) AS [MENOR A�O NACIMIENTO]
FROM animales


-- pROMEDIO DE LOS A�OS DE NACIMIENTO DE LOS ANIMLAES
SELECT AVG (annio_nacimiento) AS [PROMEDIO A�O NACIMIENTO]
FROM animales

-- PROMEDIE EL TAMA�O DE LOS ZOO

SELECT AVG (tamano) AS [PROMEDIO TAMA�O ZOOLOGICOS]
FROM zoologico
-- CUENTE CUANTOS MACHOS Y CUANTAS HEMBRAS HAY

SELECT COUNT (sexo) AS [MACHOS], sexo
FROM animales
WHERE sexo='male' OR sexo='FEMALE'
GROUP BY sexo

-- CUENTE LOS ANIMALES DE AMERICA
SELECT COUNT(*) AS [NACIDOS EN AMERICA]
FROM animales
WHERE continente='Norte America'

-- CUENTE LOS ANIMALES QUE HAYAN NACIDO DESPUES DEL 90 EN AFRICA
SELECT COUNT (*) AS [NACIDOS EN AFRICA LUEGO DE 1990]
FROM animales
WHERE continente='Africa' and annio_nacimiento>=1990

-- Cuente los animales que sean de Asia y America
SELECT COUNT(*) AS [ANIMALES DE ASIA Y AMERICA]
FROM animales
WHERE continente='Norte America' OR continente='Asia'

-- Sume los presupuestos que esten entre 100 y 200 millones

SELECT SUM(presupuesto) AS [SUMA DE PRESUPUESTOS ENTRE 100 Y 200 MILLONES]
FROM zoologico
WHERE presupuesto>=10000000 AND presupuesto<=200000000
-- Tambien se puede usar between presupuesto 10000000 and 2000000



-- PROMEDIO DE LOS A�OS DE NACIMIENTO MENORES A 2000 Y MAYORES A 2010
SELECT AVG(annio_nacimiento) AS [PROMEDIO NACIEMTO ANTES DEL 2000 Y DEPUES DEL 2010]
FROM animales 
WHERE annio_nacimiento<=2000 OR annio_nacimiento>=2010

-- Totalizar presupuestos de cada pais
SELECT SUM(presupuesto), pais AS [TOTAL PRESUPUESTO POR PAIS]
FROM zoologico
GROUP BY pais--POR GRUPO 
ORDER BY pais DESC--ORDEN DESCENTENTE


-- eNCUENTRE EL MAAYOR Y EL MENOR PRESUPUESTO DE CADA ZOO POR PAIS
-- ORDENE EL RESULTADO POR NOMBRE
SELECT MAX(presupuesto) AS [MAYOR PRESUPUESTO], MIN (presupuesto) AS [MENOR PRESUPUESTO], pais
FROM zoologico
GROUP BY pais 
ORDER BY pais 

-- ENCUENTRE ZOOS CUYO PRESUPUESTO PROMEDIO SEA MAYOR A 150 MILLONES
SELECT AVG(presupuesto) AS 'PAIS CUYO ZOO TIENE PRESUPUEST MAYOR A 150 MILLONES', nombre, pais
FROM zoologico
GROUP BY nombre, pais
HAVING AVG (presupuesto)>150000000

-- ENCUENTRE ANIMALES CUYO PROMEDIO DE NACIMIENTO ESTE ENTRE 2000 Y 2010

SELECT COUNT(annio_nacimiento) AS [NACIEMTO ENTRE 2000 Y 2010],nombre_cientifico, annio_nacimiento, id_zoologico
FROM animales 
GROUP BY nombre_cientifico, annio_nacimiento, id_zoologico
HAVING annio_nacimiento BETWEEN 2000 AND 2010	



-- CONSULTA DE ACCI�N
--Modificaci�n de la base de datos a nivel registros.

INSERT INTO zoologico
VALUES('7','Zoo Malevaje','Medellin','Colombia',6,150000000)-- Se ingresa esta informacion a la tabla zoologico y este aparece ahora en la tabla ID 7


INSERT INTO zoologico (id, nombre, ciudad,pais,tamano.presupuesto)
VALUES
('8','Zoo Gallinero','Mexico','Mexico',6,170000000)

--EJEMPLO AGREGAR 3 ESPECIES Y 6 ANIMALES

INSERT INTO especies(nombre_cientifico, nombre_vulgar, familia, peligro_extincion)
VALUES
('perritis morfis', 'perro','Canes','no'),
('caballus rayus','Zebra','De los caballos','No'),
('hipopotus','hipopotamo','hipos','si')

INSERT INTO animales(nombre_cientifico, id_zoologico, annio_nacimiento,sexo,pais_origen,continente)
VALUES
('Perritis morfis','3','2009','male','Colombia','Sur America'),
('Perritis morfis','2','2008','female','Colombia','Sur America'),
('hipopotus','1','2011','male','Africa','Africa'),
('hipopotus','4','2005','female','Africa','Africa'),
('caballus rayus','5','2006','male','Africa','Africa'),
('caballus rayus','6','1991','female','Africa','Africa')

--ACTUALIZACION DE REGISTROS 
--Actualice la ciudad de todos los zoologicos de Medell�n

UPDATE zoologico
SET ciudad = 'Medell�n'--le puse la tilde para corregir ortograficamente la palabra en la base de tados en todas las filas que estban mal escritas
WHERE ciudad = 'Medellin'


-- EJERCICIO: actualizar tamano de zoos en Colombia a 10

UPDATE zoologico
SET tamano = 10
WHERE pais='Colombia'


-- Aumentar todos los presupuestos en un 5%
UPDATE zoologico
SET presupuesto = presupuesto*0.05+presupuesto

-- Cambie el continente de los animalex de Asia por Africa

UPDATE animales
SET continente = 'Africa' 
WHERE continente = 'Asia'


--Todos los animales cuya familia comienza en la letra 'f' debe aparecer en peligro de exitincion

UPDATE especies
SET peligro_extincion = '1'
WHERE familia LIKE 'F%'

-- Cambiar el pais de origen a Colombia a todas las hembnras nacidas entre 2000 y 2005


UPDATE animales
SET pais_origen = 'Colombia'
WHERE sexo = 'female' and annio_nacimiento BETWEEN 2000 AND 2005


--ELIMINACION DE REGISTROS
--Eliminar los zoologicos de Venezuela
DELETE zoologico
WHERE pais='Venezuela'

UPDATE zoologico
SET id=1
WHERE id= 100

--Eliminar las especies cuyo nombre de la familia tyermine en S y que esten en peligro de extincion
DELETE especies
WHERE familia LIKE '%S' and peligro_extincion= 1
-- Eliminar �zoos presup > 600 millones
DELETE zoologico
WHERE presupuesto>=450000000

--***************************************************************************************************
--Modificacion de la base de datos a nivel estructural

--Crear bse de DATOS
CREATE DATABASE zoo_jsoh_2

USE zoo_jsoh_2-- Usa otra BD
USE zoojsocampo

DROP DATABASE zoo_jsoh_2--Eliminar estructuras OJO

--Creamos la tabla segun especificaciones
CREATE TABLE visitas(
id VARCHAR(50) NOT NULL PRIMARY KEY, 
fecha DATE NOT NULL,
cantidad_personas INTEGER NOT NULL,
valor_recaudo MONEY NOT NULL,
id_zoo VARCHAR(50) NOT NULL,
FOREIGN KEY (id_zoo) REFERENCES zoologico(id)
)

-- Agregar un campo (Columna) a una tabla
-- Addicionar el campo cantidad_menores_edad
ALTER TABLE visitas
ADD cantidad_menores INTEGER NULL
 
 -- Cambiar el campo del nombre del campo adicionado arriba
 ALTER TABLE visitas
 DROP COLUMN cantidad_menores

 --MODIFICAR un campo: cambiar 
 ALTER TABLE visitas
 ALTER COLUMN cantidad_menores real NOT NULL

 --cambiar nombre de una tabla
 EXEC sp_rename  'visitas','visitas_zoo'
 GO

 --cambiar nombre de una columna
 EXEC sp_rename 'visitas_zoo.cantidad_menores', 'visitas_zoo.cantidad_menores_edad'--el punto define el nombre de la tabla y el nombre que se quiere cambiar
 GO
 --Elimanr una tabla
 DROP TABLE visitas_zoo