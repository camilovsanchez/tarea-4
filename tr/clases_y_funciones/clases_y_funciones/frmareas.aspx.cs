﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace clases_y_funciones
{
    public partial class frmareas : System.Web.UI.Page
    {
        ClsAreas objareas = new ClsAreas();
        ClsPerimetro objperimetro = new ClsPerimetro();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnareacir_Click(object sender, EventArgs e)
        {
            double rad, resultado;
            rad = Convert.ToDouble(txtradio.Text);
            resultado = objareas.calcular_area_cir(rad);
            lblareacir.Text = Convert.ToString(resultado);

        }

        protected void btnarearec_Click(object sender, EventArgs e)
        {
            int largo, ancho, resultado;
            largo = Convert.ToInt32(txtlargo.Text);
            ancho = Convert.ToInt32(txtancho.Text);
            resultado = objareas.calcular_area_rectangulo(largo, ancho);
            lblarearec.Text = Convert.ToString(resultado);
        }

        protected void btnareacua_Click(object sender, EventArgs e)
        {
            double lado, resultado;
            lado = Convert.ToDouble(txtlado.Text);
            resultado = objareas.calcular_area_cuadrado(lado);
            lblareacuad.Text = Convert.ToString(resultado);


        }

        protected void btnpercir_Click(object sender, EventArgs e)
        {
            double radio, resultado;
            radio = Convert.ToDouble(txtradio.Text);
            resultado = objperimetro.calcular_perimetro_circ(radio);
            lblpercir.Text = Convert.ToString(resultado);
        }

        protected void btnperrec_Click(object sender, EventArgs e)
        {
            int largo, ancho, resultado;
            largo = Convert.ToInt32(txtlargo.Text);
            ancho = Convert.ToInt32(txtancho.Text);
            resultado = objperimetro.calcular_perimetro_rec(largo, ancho);
            lblperrec.Text = Convert.ToString(resultado);
        }

        protected void btnpercuad_Click(object sender, EventArgs e)
        {
            double lado, resultado;
            lado = Convert.ToDouble(txtlado.Text);
            resultado = objperimetro.calcular_perimetro_cuad(lado);
            lblpercuad.Text = Convert.ToString(resultado);
        }
    }
}