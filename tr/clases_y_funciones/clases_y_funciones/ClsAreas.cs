﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clases_y_funciones
{
    public class ClsAreas
    {
        public double calcular_area_cir(double radio)
        {
            double areacir;
            areacir = Math.PI * radio * radio;
            return areacir;
        }

        public int calcular_area_rectangulo (int largo, int ancho)
        {
            int arearec;
            arearec = largo * ancho;
            return arearec;
        }

        public double calcular_area_cuadrado (double lado)
        {
            double areacuad;
            areacuad = lado * lado;
            return areacuad;
        }
    }
}