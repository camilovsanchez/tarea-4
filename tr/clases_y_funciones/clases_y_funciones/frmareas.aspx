﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmareas.aspx.cs" Inherits="clases_y_funciones.frmareas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            width: 383px;
        }
        .auto-style4 {
            height: 23px;
            width: 383px;
        }
        .auto-style5 {
            height: 19px;
            width: 383px;
        }
        .auto-style6 {
            height: 19px;
        }
        .auto-style7 {
            height: 19px;
            width: 324px;
        }
        .auto-style8 {
            width: 324px;
        }
        .auto-style9 {
            height: 23px;
            width: 324px;
        }
        .auto-style10 {
            width: 383px;
            height: 26px;
        }
        .auto-style11 {
            width: 324px;
            height: 26px;
        }
        .auto-style12 {
            height: 26px;
        }
        .auto-style13 {
            height: 19px;
            width: 271px;
        }
        .auto-style14 {
            height: 26px;
            width: 271px;
        }
        .auto-style15 {
            width: 271px;
        }
        .auto-style16 {
            height: 23px;
            width: 271px;
        }
        .auto-style17 {
            width: 383px;
            height: 30px;
        }
        .auto-style18 {
            width: 324px;
            height: 30px;
        }
        .auto-style19 {
            width: 271px;
            height: 30px;
        }
        .auto-style20 {
            height: 30px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style5">Círculo</td>
                <td class="auto-style7"></td>
                <td class="auto-style13"></td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td class="auto-style10">Radio</td>
                <td class="auto-style11">
                    <asp:TextBox ID="txtradio" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style14"></td>
                <td class="auto-style12"></td>
            </tr>
            <tr>
                <td class="auto-style17">
                    <asp:Button ID="btnareacir" runat="server" OnClick="btnareacir_Click" Text="Área círculo" />
                </td>
                <td class="auto-style18">
                    <asp:Label ID="lblareacir" runat="server"></asp:Label>
                </td>
                <td class="auto-style19">
                    <asp:Button ID="btnpercir" runat="server" Text="Perimetro círculo" OnClick="btnpercir_Click" />
                </td>
                <td class="auto-style20">
                    <asp:Label ID="lblpercir" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Rectángulo</td>
                <td class="auto-style8">&nbsp;</td>
                <td class="auto-style15">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Largo</td>
                <td class="auto-style8">
                    <asp:TextBox ID="txtlargo" runat="server"></asp:TextBox>
&nbsp;</td>
                <td class="auto-style15">Ancho</td>
                <td>
                    <asp:TextBox ID="txtancho" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Button ID="btnarearec" runat="server" OnClick="btnarearec_Click" Text="Área rectángulo" />
                </td>
                <td class="auto-style9">
                    <asp:Label ID="lblarearec" runat="server"></asp:Label>
                </td>
                <td class="auto-style16">
                    <asp:Button ID="btnperrec" runat="server" Text="Perimetro rectángulo" OnClick="btnperrec_Click" />
                </td>
                <td class="auto-style2">
                    <asp:Label ID="lblperrec" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Cuadrado</td>
                <td class="auto-style8">&nbsp;</td>
                <td class="auto-style15">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">Lado</td>
                <td class="auto-style8">
                    <asp:TextBox ID="txtlado" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style15">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Button ID="btnareacua" runat="server" Text="Área cuadrado" OnClick="btnareacua_Click" />
                </td>
                <td class="auto-style8">
                    <asp:Label ID="lblareacuad" runat="server"></asp:Label>
                </td>
                <td class="auto-style15">
                    <asp:Button ID="btnpercuad" runat="server" Text="Perimetro cuadrado" OnClick="btnpercuad_Click" />
                </td>
                <td>
                    <asp:Label ID="lblpercuad" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
