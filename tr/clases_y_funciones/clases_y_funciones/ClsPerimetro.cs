﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace clases_y_funciones
{
    public class ClsPerimetro
    {
        public double calcular_perimetro_circ(double radio)
        {
            double perimetrocir;
            perimetrocir = 2 * Math.PI * radio;
            return perimetrocir;
        }
           public int calcular_perimetro_rec (int largo, int ancho)
        {
            int perimetrorec;
            perimetrorec = (largo + ancho) * 2;
            return perimetrorec;
        }

        public double calcular_perimetro_cuad (double lado)
        {
            double perimetrocuad;
            perimetrocuad = lado * 4;
            return perimetrocuad;
        }
    }
}