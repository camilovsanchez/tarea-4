--Mostrar todos los zoologicos
SELECT * 
FROM zoologico

--Mostrar zoologicos por pais. LISTAR SOLO EL NOMBRE Y LA CIUDAD DE ESTOS
SELECT *
FROM zoologico
WHERE pais='Colombia'


-- LISTAR ESPECIES EN PELIGRO DE EXTINCION

SELECT *
FROM especie
WHERE peligro_extincion= 1


--Listar los zoo cuyo presupuesto no supera los 70 millones

SELECT *
FROM zoologico
WHERE presupuesto>100000000	


--  Listar los zoo que tengan un tama�o entre 4 y 6 km2
SELECT *
FROM zoologico
WHERE tama�o >4 and tama�o < 8

SELECT *
FROM zoologico
WHERE tama�o BETWEEN 4 AND 8


-- LISTADO DE LOS ZOO POR CIUDAD EN ORDEN ALFABETICO
SELECT *
FROM zoologico
ORDER BY ciudad DESC


-- ORDENAR POR CIUDD Y NOMBRE
SELECT *
FROM zoologico
ORDER BY ciudad, nombre


-- LISTAR ZOOLOGICOS CUYO NOMBRE EMPIEZA POR "L"
SELECT *
FROM zoologico
WHERE nombre LIKE 'l%'

--COUNT cuenta los registros
-- SUM suma de valores de la columna
--AVG Promedia los valores de la columna
--MAX encuentra el valor m�ximo de la columna
--MIN encuentra el valor minimo de la columna

-- COUNT: cUENTE LOS REGISTROS DE LA TABLA ANIMALES

SELECT COUNT (*) AS [Total animales BD]
FROM animal

-- CUENTE LOS ZOO QUE TIENE COLOMBIA
SELECT COUNT (*) AS [Total zoo Colombia]
FROM zoologico
WHERE pais='Colombia'
-- TOTALIZAR EL PRESUPUESTO DE TODOS LOS ZOO
SELECT SUM (presupuesto) AS [Presupuesto Zool�gicos]
FROM zoologico

-- Promediar los presupuestos de los zoo
SELECT SUM (presupuesto)/ COUNT(id) AS [Promedio aritmetico], AVG(presupuesto) AS[promedio AVG]
FROM zoologico

-- Encontrar el Mayor presupuesto dado a un zoo
SELECT MAX(presupuesto) AS [MAYOR PRESUPUESTO]
FROM zoologico

-- mOSTRAR EL MENOR TAMA�O DE LOS ZOO
SELECT MIN (tama�o) AS [MENOR TAMA�O]
FROM zoologico

-- MOSTRAR EL MAYOR Y MENOR A�O DE NACIMIENTO DE LOS ANIMALES
SELECT MAX(annio_nacimiento) AS [MAYOR A�O NACIMIENTO], MIN(annio_nacimiento) AS [MENOR A�O NACIMIENTO]
FROM animal


-- pROMEDIO DE LOS A�OS DE NACIMIENTO DE LOS ANIMLAES
SELECT AVG (annio_nacimiento) AS [PROMEDIO A�O NACIMIENTO]
FROM animal
-- PROMEDIE EL TAMA�O DE LOS ZOO

SELECT AVG (tama�o) AS [PROMEDIO TAMA�O ZOOLOGICOS]
FROM zoologico
-- CUENTE CUANTOS MACHOS Y CUANTAS HEMBRAS HAY

SELECT COUNT (sexo) AS [MACHOS], sexo
FROM animal
WHERE sexo='male' OR sexo='FEMALE'
GROUP BY sexo

-- CUENTE LOS ANIMALES DE AMERICA
SELECT COUNT(*) AS [NACIDOS EN AMERICA]
FROM animal
WHERE continente='Norte America'

-- CUENTE LOS ANIMALES QUE HAYAN NACIDO DESPUES DEL 90 EN AFRICA
SELECT COUNT (*) AS [NACIDOS EN AFRICA LUEGO DE 1990]
FROM animal
WHERE continente='Africa' and annio_nacimiento>=1990

-- Cuente los animales que sean de Asia y America
SELECT COUNT(*) AS [ANIMALES DE ASIA Y AMERICA]
FROM animal
WHERE continente='Norte America' OR continente='Asia'

-- Sume los presupuestos que esten entre 100 y 200 millones

SELECT SUM(presupuesto) AS [SUMA DE PRESUPUESTOS ENTRE 100 Y 200 MILLONES]
FROM zoologico
WHERE presupuesto>=10000000 AND presupuesto<=200000000
-- Tambien se puede usar between presupuesto 10000000 and 2000000



-- PROMEDIO DE LOS A�OS DE NACIMIENTO MENORES A 2000 Y MAYORES A 2010
SELECT AVG(annio_nacimiento) AS [PROMEDIO NACIEMTO ANTES DEL 2000 Y DEPUES DEL 2010]
FROM animal 
WHERE annio_nacimiento<=2000 OR annio_nacimiento>=2010

-- Totalizar presupuestos de cada pais
SELECT SUM(presupuesto), pais AS [TOTAL PRESUPUESTO POR PAIS]
FROM zoologico
GROUP BY pais--POR GRUPO 
ORDER BY pais DESC--ORDEN DESCENTENTE


-- eNCUENTRE EL MAAYOR Y EL MENOR PRESUPUESTO DE CADA ZOO POR PAIS
-- ORDENE EL RESULTADO POR NOMBRE
SELECT MAX(presupuesto) AS [MAYOR PRESUPUESTO], MIN (presupuesto) AS [MENOR PRESUPUESTO], pais
FROM zoologico
GROUP BY pais 
ORDER BY pais 

-- ENCUENTRE ZOOS CUYO PRESUPUESTO PROMEDIO SEA MAYOR A 150 MILLONES
SELECT AVG(presupuesto) AS 'PAIS CUYO ZOO TIENE PRESUPUEST MAYOR A 150 MILLONES', nombre, pais
FROM zoologico
GROUP BY nombre, pais
HAVING AVG (presupuesto)>150000000

-- ENCUENTRE ANIMALES CUYO PROMEDIO DE NACIMIENTO ESTE ENTRE 2000 Y 2010

SELECT COUNT(annio_nacimiento) AS [NACIEMTO ENTRE 2000 Y 2010],nombre_cientifico_especie, annio_nacimiento, id_zoologico
FROM animal 
GROUP BY nombre_cientifico_especie, annio_nacimiento, id_zoologico
HAVING annio_nacimiento BETWEEN 2000 AND 2010	



-- CONSULTA DE ACCI�N
--Modificaci�n de la base de datos a nivel registros.

INSERT INTO zoologico
VALUES('7','Zoo Malevaje','Medellin','Colombia',6,150000000)-- Se ingresa esta informacion a la tabla zoologico y este aparece ahora en la tabla ID 7


INSERT INTO zoologico (id, nombre, ciudad,pais,tamano.presupuesto)
VALUES
('8','Zoo Gallinero','Mexico','Mexico',6,170000000)

--EJEMPLO AGREGAR 3 ESPECIES Y 6 ANIMALES

INSERT INTO especie(nombre_cient�fico, nombre_vulgar, familia, peligro_extincion)
VALUES
('perritis morfis', 'perro','Canes','no'),
('caballus rayus','Zebra','De los caballos','No'),
('hipopotus','hipopotamo','hipos','si')

INSERT INTO animal(nombre_cientifico_especie, id_zoologico, annio_nacimiento,sexo,pais_origen,continente)
VALUES
('Perritis morfis','3','2009','male','Colombia','Sur America'),
('Perritis morfis','2','2008','female','Colombia','Sur America'),
('hipopotus','1','2011','male','Africa','Africa'),
('hipopotus','4','2005','female','Africa','Africa'),
('caballus rayus','5','2006','male','Africa','Africa'),
('caballus rayus','6','1991','female','Africa','Africa')

--ACTUALIZACION DE REGISTROS 
--Actualice la ciudad de todos los zoologicos de Medell�n

UPDATE zoologico
SET ciudad = 'Medell�n'--le puse la tilde para corregir ortograficamente la palabra en la base de tados en todas las filas que estban mal escritas
WHERE ciudad = 'Medellin'


-- EJERCICIO: actualizar tamano de zoos en Colombia a 10

UPDATE zoologico
SET tama�o = 10
WHERE pais='Colombia'


-- Aumentar todos los presupuestos en un 5%
UPDATE zoologico
SET presupuesto = presupuesto*0.05+presupuesto

-- Cambie el continente de los animalex de Asia por Africa

UPDATE animal
SET continente = 'Africa' 
WHERE continente = 'Asia'


--Todos los animales cuya familia comienza en la letra 'f' debe aparecer en peligro de exitincion

UPDATE especie
SET peligro_extincion = '1'
WHERE familia LIKE 'F%'

-- Cambiar el pais de origen a Colombia a todas las hembnras nacidas entre 2000 y 2005


UPDATE animal
SET pais_origen = 'Colombia'
WHERE sexo = 'female' and annio_nacimiento BETWEEN 2000 AND 2005


--ELIMINACION DE REGISTROS
--Eliminar los zoologicos de Venezuela
DELETE zoologico
WHERE pais='Venezuela'

UPDATE zoologico
SET id=1
WHERE id= 100

--Eliminar las especies cuyo nombre de la familia tyermine en S y que esten en peligro de extincion
DELETE especie
WHERE familia LIKE '%S' and peligro_extincion= 1

-- Eliminar �zoos presup > 600 millones
DELETE zoologico
WHERE presupuesto>=450000000

--***************************************************************************************************
--Modificacion de la base de datos a nivel estructural

--Crear bse de DATOS
CREATE DATABASE zoo_jsoh_2

USE zoo_jsoh_2-- Usa otra BD
USE zoojsocampo

DROP DATABASE zoo_jsoh_2--Eliminar estructuras OJO

--Creamos la tabla segun especificaciones
CREATE TABLE visitas(
id VARCHAR(50) NOT NULL PRIMARY KEY, 
fecha DATE NOT NULL,
cantidad_personas INTEGER NOT NULL,
valor_recaudo MONEY NOT NULL,
id_zoo VARCHAR(50) NOT NULL,
FOREIGN KEY (id_zoo) REFERENCES zoologico(id)
)

-- Agregar un campo (Columna) a una tabla
-- Addicionar el campo cantidad_menores_edad
ALTER TABLE visitas
ADD cantidad_menores INTEGER NULL
 
 -- Cambiar el campo del nombre del campo adicionado arriba
 ALTER TABLE visitas
 DROP COLUMN cantidad_menores

 --MODIFICAR un campo: cambiar 
 ALTER TABLE visitas
 ALTER COLUMN cantidad_menores real NOT NULL

 --cambiar nombre de una tabla
 EXEC sp_rename  'visitas','visitas_zoo'
 GO

 --cambiar nombre de una columna
 EXEC sp_rename 'visitas_zoo.cantidad_menores', 'visitas_zoo.cantidad_menores_edad'--el punto define el nombre de la tabla y el nombre que se quiere cambiar
 GO
 --Elimanr una tabla
 DROP TABLE visitas_zoo

 -- Listar los registros de los zool�gicos y especies
 SELECT z.*, e.*
 FROM zoologico z, especies e

 -- Listar todas las especies y los animales que pertenecen a ella
 SELECT e.*, a.*
 FROM especies e, animales a
 WHERE e.nombre_cientifico = a.nombre_cientifico_especie

 -- Listar todas las especies y los animales que pertenecen. Debe mostrar las especies que no tengan animales asociados
SELECT e.*, a.*
From especies e INNER JOIN animales a
	ON e.nombre_cientifico = a.nombre_cientifico_especie

SELECT e.*, a.*
FROM especies e LEFT JOIN animales a
	ON e.nombre_cientifico = a.nombre_cientifico_especie

-- Operaci�n UNION
SELECT nombre_cientifico
FROM especies
UNION 
SELECT animal.especie, animal.pais_origen
FROM animales

-- Operacion INTERSECT
SELECT nombre_cientifico
FROM especies
INTERSECT
SELECT animales.especie, animal.pais_origen
FROM animales

-- Muestre los animales y el zool�gico al que pertenecen 
SELECT a.*, z.nombre AS [zool�gico]
FROM animales a INNER JOIN zoologico z
ON a.id_zoologico = z.id

-- Muestre los animales y el zoologico al que p�rtenecen siempre y cuando el presupuesto de este no est� entre 100 y 200 millones
SELECT a.*, z.nombre AS [zool�gico]
FROM animales a INNER JOIN zoologico z
ON a.id_zoologico = z.id
WHERE not z.presupuesto >= 2000000 and z.presupuesto <= 10000000

-- Liste los animales con su nombre vulgar
-- Liste los animales con su nombre vulgar de especie que no est�n en peligro de extincion
-- Muestre los animales indicando el nombre vulgar de su especie y el nombre del zoologico donde se encuentra
SELECT a.annio_nacimiento, a.pais_origen, a.sexo, e.nombre_vulgar AS [Nombre vulgar], z.nombre [Zool�gico]
FROM especie e INNER JOIN animal a
	ON (e.nombre_cientifico = a.nombre_cientifico_especie)
INNER JOIN zoologico z
	ON	 (z.id = a.id_zoologico)

--Crear un SP para listar los zool�gicos con todos sus campos
CREATE PROCEDURE sp_zoo_list
AS
	SELECT *
	FROM zoologico;

EXEC sp_zoo_list;

-- Mostrar todos los registros de zool�gicos con todos sus campos si el nombre de este coincide con el dato indicado por el usuario, 
--en caso contrario, muestre solo la ciudad de todos los zool�gicos

CREATE PROCEDURE sp_zoo_conditional
	@nombre VARCHAR (50)
AS
	IF @nombre = 'Zoo Cali'
		BEGIN
			SELECT *
			FROM zoologico
			WHERE nombre = @nombre;
		END;
	ELSE
		BEGIN
			SELECT zoologico.ciudad
			FROM zoologico
		END;
EXEC sp_zoo_conditional 'Zoo Cali';

-- La misma consulta pero con sub procedimiento
CREATE PROCEDURE sp_zoo_conditional2
	@nombre VARCHAR (50)
AS
	IF @nombre = (SELECT nombre FROM zoologico WHERE nombre=@nombre)
		BEGIN
			SELECT *
			FROM zoologico
			WHERE nombre = @nombre;
		END;
	ELSE
		BEGIN
			SELECT zoologico.ciudad
			FROM zoologico
		END;
EXEC sp_zoo_conditional2 'Zoo Cali';

-- Listar los zool�gicos de Buenos Aires
CREATE PROCEDURE sp_zoo_ciudad
@ciudad VARCHAR (20)
AS
	SELECT *
	FROM zoologico
	WHERE ciudad = @ciudad;

EXEC sp_zoo_ciudad 'Buenos Aires DC';

-- Crear un SP para insertar zool�gicos
CREATE PROCEDURE sp_zoo_insertar
	@id VARCHAR (50),
	@nombre VARCHAR (50),
	@pais VARCHAR (50),
	@ciudad VARCHAR (50),
	@tama�o	DECIMAL (18, 0),
	@presupuesto MONEY

	AS
		BEGIN 
			INSERT INTO zoologico (id, nombre, pais, ciudad, tama�o, presupuesto) VALUES (@id, @nombre, @pais, @ciudad, @tama�o, @presupuesto)	
		END
EXEC sp_zoo_insertar '9', 'Cami', 'Medell�n', 'Colombia', 18, 1000000;

-- Crear un SP para actualizar en un 10% los presupuestos de los zool�gicos de un pa�s dado
CREATE PROCEDURE sp_zoo_presupuesto
	@pais VARCHAR (50)

	AS
	UPDATE zoologico
	SET presupuesto = 1.1 * presupuesto
	WHERE pais = @pais;

EXEC sp_zoo_presupuesto 'Colombia'


-- Crear un SP para eliminar zool�gicos por un id dado
CREATE PROCEDURE sp_zoo_eliminar
	@id VARCHAR (50)
AS
DELETE FROM zoologico
WHERE id = @id;

EXEC sp_zoo_eliminar '8'

-- Crear una vista para mostrar los zool�gicos

CREATE VIEW vista_zoo
AS
SELECT * FROM zoologico

SELECT * FROM vista_zoo

-- Crear una vista para mostrar los animales que hayan nacido despu�s del 2010. Los campos de la vista deben ser renombrados