create procedure sp_guardar_paciente
@pidpaciente nvarchar(10),
@pnompaciente nvarchar (50),
@pdirpaciente nvarchar (50),
@ptelpaciente nvarchar (10)

as
	if not exists (select idpaciente from tblpaciente where idpaciente = @pidpaciente)
	insert into tblpaciente (idpaciente, nompaciente, dirpaciente, telpaciente)
	values (@pidpaciente, @pnompaciente, @pdirpaciente, @ptelpaciente)