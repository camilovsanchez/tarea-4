-- Crear tabla

CREATE TABLE Empleados
(
Id INT Identity,
Nombre Varchar(100),
Salario Decimal (10,2)
)
INSERT INTO Empleados VALUES ('Anita',1000);
INSERT INTO Empleados VALUES ('Roberto',1200);
INSERT INTO Empleados VALUES ('John',1100);
INSERT INTO Empleados VALUES ('Stefania',1300);
INSERT INTO Empleados VALUES ('Maria',1400);

-- Tabla auditada

CREATE TABLE EmpleadosAudit
(
Id int,
Nombre varchar(100),
Salario decimal (10,2),
Audit_Action varchar(100),
Audit_Timestamp datetime
)

--SUBCONSULTAS

-- 1

SELECT *
FROM empleados
WHERE id in
           (SELECT id
            FROM empleados
            WHERE id = 7);

-- 2
SELECT nombre, salario
FROM empleados
WHERE salario in (SELECT MIN (salario) FROM empleados
					GROUP BY id);

-- 3




-- TRIGGERS
-- INSERT TRIGGER
CREATE TRIGGER TriggerInsertar ON [dbo].[Empleados] 
FOR INSERT
AS
	declare @empid int;
	declare @empname varchar(100);
	declare @empsal decimal(10,2);
	declare @audit_action varchar(100);

	select @empid=i.Id from inserted i;	
	select @empname=i.Nombre from inserted i;	
	select @empsal=i.Salario from inserted i;	
	set @audit_action='Se ingres� un nuevo empleado';

	insert into EmpleadosAudit
           (Id, Nombre, Salario,Audit_Action,Audit_Timestamp) 
	values(@empid,@empname,@empsal,@audit_action,getdate());

	PRINT 'Se ha ingresado un nuevo usuario. Insert trigger activado'
GO

-- Insertar nuevo usuario
insert into Empleados values('Pedro',1700);

-- UPDATE TRIGGER

CREATE TRIGGER TriggerUpdate ON [dbo].[Empleados] 
FOR UPDATE
AS
	declare @empid int;
	declare @empname varchar(100);
	declare @empsal decimal(10,2);
	declare @audit_action varchar(100);

	select @empid=i.Id from inserted i;	
	select @empname=i.Nombre from inserted i;	
	select @empsal=i.Salario from inserted i;	
	
	if update(Nombre)
		set @audit_action='Se actualiz� el nombre del empleado.';
	if update(Salario)
		set @audit_action='Se actualiz� el salario del empleado.';

	insert into EmpleadosAudit(Id,Nombre,Salario,Audit_Action,Audit_Timestamp) 
	values(@empid,@empname,@empsal,@audit_action,getdate());

	PRINT 'Se han modificado datos. Update trigger activado'
GO

-- Actualizar datos
update Empleados set Salario=1550 where Id=4


-- DELETE TRIGGER

CREATE TRIGGER DeleteTrigger ON [dbo].[Empleados] 
AFTER DELETE
AS
	declare @empid int;
	declare @empname varchar(100);
	declare @empsal decimal(10,2);
	declare @audit_action varchar(100);

	select @empid=d.Id from deleted d;	
	select @empname=d.Nombre from deleted d;	
	select @empsal=d.Salario from deleted d;	
	set @audit_action='Se ha eliminado un registro. Trigger activado';

	insert into EmpleadosAudit
(Id,Nombre,Salario,Audit_Action,Audit_Timestamp) 
	values(@empid,@empname,@empsal,@audit_action,getdate());

	PRINT 'Se ha eliminado un registro. Trigger activado'
GO