﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Promedio_vector_enteros
{
    public class ClsVector
    {
        public int [] llenar_vector (int n)
        {
            //Definición de vectores
            Random rnd = new Random();
            int[] enteros = new int[n];
            int j;
            for (j = 0; j <= n; j++)
            {
                enteros[j] = rnd.Next(1000);
            }
            return enteros;
        }   

        public double calcular_promedio (int[] vec, int n)
        {
            int  k;
            double suma = 0, prom;
            for (k = 0; k < n; k ++)
            {
                suma = suma + vec[k];
            }

            prom = suma / n;
            return prom;
                
        }

        public int cantidad_numeros (int[] vec, int n, double promedio)
        {
            int k, cant = 0;

            for (k = 0; k < n; k ++)
            {
                if (vec[k] > promedio)
                    cant++;
            }
            return cant;
        }
    } 
}