﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmVector.aspx.cs" Inherits="Promedio_vector_enteros.frmVector" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 652px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Tamaño del vector</td>
                <td>
                    <asp:TextBox ID="txtn" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Promedio</td>
                <td>
                    <asp:Label ID="lblpromedio" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Cantidad de elementos mayores que el promedio</td>
                <td>
                    <asp:Label ID="lblmayores" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btncalcular" runat="server" OnClick="btncalcular_Click" Text="Calcular" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
