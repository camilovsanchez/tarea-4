﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Promedio_vector_enteros
{
    public partial class frmVector : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btncalcular_Click(object sender, EventArgs e)
        {
            //Definición de variables
            ClsVector objvector = new ClsVector();
            int n, cant;
            double promedio;

            //Lectura de datos
            n = Convert.ToInt32(txtn.Text);
            int[] enteros = new int[n];
            enteros = objvector.llenar_vector(n);
            lblpromedio.Text = "" + promedio;
            cant = objvector.cantidad_numeros(enteros, n, promedio);
            lblmayores.Text = "" + cant;
        }
    }
}