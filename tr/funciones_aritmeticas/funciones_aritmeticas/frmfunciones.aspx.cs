﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace funciones_aritmeticas
{
    public partial class frmfunciones : System.Web.UI.Page
    {
        int resultado;
        double num1, num2, potencia, division;


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnredondeo_Click(object sender, EventArgs e)
        {
            num1 = Convert.ToDouble(txtnum1.Text);
            num2 = Convert.ToDouble(txtnum2.Text);
            division = num1 / num2;
            double resultado1 = Math.Round(division, 2);
            lblredondeo.Text = Convert.ToString(resultado1);
        }
    

        protected void btnaleatorio_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            resultado = rnd.Next(1000);
            lblaleatorio.Text = Convert.ToString(resultado);


        }

        protected void btnpotencia_Click(object sender, EventArgs e)
        {
            num1 = Convert.ToDouble(txtnum1.Text);
            num2 = Convert.ToDouble(txtnum2.Text);
            potencia = Math.Pow(num1, num2);
            lblpotencia.Text = Convert.ToString(resultado);
        }
    }
}