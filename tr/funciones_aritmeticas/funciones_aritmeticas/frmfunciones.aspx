﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmfunciones.aspx.cs" Inherits="funciones_aritmeticas.frmfunciones" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 651px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Numero uno</td>
                <td>
                    <asp:TextBox ID="txtnum1" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Numero dos</td>
                <td>
                    <asp:TextBox ID="txtnum2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnaleatorio" runat="server" OnClick="btnaleatorio_Click" Text="Aleatorio" />
                </td>
                <td>
                    <asp:Label ID="lblaleatorio" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnpotencia" runat="server" OnClick="btnpotencia_Click" Text="Potencia" />
                </td>
                <td>
                    <asp:Label ID="lblpotencia" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnredondeo" runat="server" OnClick="btnredondeo_Click" Text="Redondeo" />
                </td>
                <td>
                    <asp:Label ID="lblredondeo" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
