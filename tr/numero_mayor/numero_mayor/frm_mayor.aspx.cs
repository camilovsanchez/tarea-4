﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace numero_mayor
{
    public partial class frm_mayor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnmayor_Click(object sender, EventArgs e)
        {
            //Definición de variables
            ClsMayor objmayor = new ClsMayor();
            int n1, n2, n3, n4, may1, may2, mayor;

            //Lectura de datos
            n1 = Convert.ToInt32(txtnum1.Text);
            n2 = Convert.ToInt32(txtnum2.Text);
            n3 = Convert.ToInt32(txtnum3.Text);
            n4 = Convert.ToInt32(txtnum4.Text);

            //Operación
            may1 = objmayor.hallar_mayor(n1, n2);
            may2 = objmayor.hallar_mayor(n3, n4);
            mayor = objmayor.hallar_mayor(may1, may2);
            lblmayor.Text = "" + mayor;
            
           
        }
    }
}