﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frm_mayor.aspx.cs" Inherits="Mayor_numero.frm_mayor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>Numero uno</td>
                <td>
                    <asp:TextBox ID="txtnum1" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Numero dos</td>
                <td>
                    <asp:TextBox ID="txtnum2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Numero tres</td>
                <td>
                    <asp:TextBox ID="txtnum3" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Mayor</td>
                <td>
                    <asp:Label ID="lblmayor" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnmayor" runat="server" OnClick="Button1_Click" Text="Mayor" />
                </td>
                <td class="auto-style2"></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
