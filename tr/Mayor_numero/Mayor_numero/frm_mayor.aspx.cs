﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mayor_numero
{
    public partial class frm_mayor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //definicion de variables
            int num1, num2, num3;

            //lectura de informacion
            num1 = Convert.ToInt32(txtnum1.Text);
            num2 = Convert.ToInt32(txtnum2.Text);
            num3 = Convert.ToInt32(txtnum3.Text);

            //operacion

            if (num1 > num2 && num1 > num3)
            {
                lblmayor.Text = "El mayor es " + num1;
            }
            else
            {
                if (num2 > num3)
                {
                    lblmayor.Text = "El mayor es " + num2;
                }
                else
                {
                    lblmayor.Text = "El mayor es " + num3;
                }
            }
        }
    }
}