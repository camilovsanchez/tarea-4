
-- Tabla
CREATE TABLE Empleados
(
Id INT Identity,
Nombre Varchar(100),
Salario Decimal (10,2)
)

INSERT INTO Empleados VALUES ('Anita',1000);
INSERT INTO Empleados VALUES ('Roberto',1200);
INSERT INTO Empleados VALUES ('John',1100);
INSERT INTO Empleados VALUES ('Stefania',1300);
INSERT INTO Empleados VALUES ('Maria',1400);

-- Tabla auditada

CREATE TABLE EmpleadosAudit
(
Id int,
Nombre varchar(100),
Salario decimal (10,2),
Audit_Action varchar(100),
Audit_Timestamp datetime
)

-- Trigger insert
CREATE TRIGGER TriggerInsertar ON [dbo].[Empleados] 
FOR INSERT
AS
	declare @empid int;
	declare @empname varchar(100);
	declare @empsal decimal(10,2);
	declare @audit_action varchar(100);

	select @empid=i.Id from inserted i;	
	select @empname=i.Nombre from inserted i;	
	select @empsal=i.Salario from inserted i;	
	set @audit_action='Se ingres� un nuevo empleado';

	insert into EmpleadosAudit
           (Id, Nombre, Salario,Audit_Action,Audit_Timestamp) 
	values(@empid,@empname,@empsal,@audit_action,getdate());

	PRINT 'Se ha ingresado un nuevo usuario. Trigger activado'
GO

-- Insertar nuevo usuario
insert into Empleados values('Karen',1500);